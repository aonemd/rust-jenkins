FROM ekidd/rust-musl-builder as builder

WORKDIR /home/rust/

ENV BUILD_NAME=rust-jenkins

# build the dependencies to utilize docker image caching for next builds
# Cargo will not build dependencies of a project that does not contain a
# main.rs, so we will create a minimal one in the src folder so that we can
# build the dependencies
COPY Cargo.toml Cargo.lock ./
RUN echo "fn main() {}" > src/main.rs

# we need to touch our real main.rs as the COPY command in docker does not
# affect the timestamps of the files it copies
COPY . .
RUN sudo touch src/main.rs

RUN cargo test
RUN cargo build --release

# strip the binary of anything that is not needed to run it. Please be aware
# that this might not always be desired as it removes any debug information as
# well
RUN strip ./target/x86_64-unknown-linux-musl/release/rust-jenkins

ENTRYPOINT ["./target/x86_64-unknown-linux-musl/release/rust-jenkins"]

FROM scratch
WORKDIR /home/rust/
COPY --from=builder /home/rust/target/x86_64-unknown-linux-musl/release/rust-jenkins .
ENTRYPOINT ["./rust-jenkins"]
